import math
import numpy as np

def moving_average(ls, num):
    cumsum = np.cumsum(np.insert(ls, 0, 0)) 
    res = (cumsum[num:] - cumsum[:-num]) / num
    return res.tolist()

def market_daily_return(close_list):
    # using Tayler Approximation
    def func():
        for idx, val in enumerate(close_list):
            if idx == 0:
                yield 0
            else:
                yield math.log(close_list[idx] / close_list[idx - 1])
    return list(func())

def market_cum_return(close_list):
    def func():
        res = 0
        num = len(close_list)
        for idx, val in enumerate(close_list):
            yield res
            if idx < num - 1:
                res = res + math.log(close_list[idx + 1] / close_list[idx])
    return list(func())

def strategy_daily_return(market_daily, signal_list):
    def func():
        num = len(market_daily)
        for idx, val in enumerate(market_daily):
            if idx == 0:
                yield 0
            else:
                yield signal_list[idx - 1] * val
    return list(func())

def strategy_cum_return(market_daily, signal_list):
    def func():
        res = 0
        num = len(market_daily)
        for idx, val in enumerate(market_daily):
            if idx == 0:
                pass
            else:
                res = res + signal_list[idx - 1] * val
            yield res
    return list(func())

def annualized_return(strategy_daily):
    return sum(strategy_daily) / len(strategy_daily) * 250

def benchmark_return(market_daily):
    return sum(market_daily) / len(market_daily) * 250

def beta(strategy_daily, market_daily):
    a = strategy_daily
    b = market_daily
    return np.cov(a,b)[0][1]/np.var(b)

def alpha(r_strategy, r_f, beta, r_market):
    return r_strategy - r_f - beta * (r_market - r_f)