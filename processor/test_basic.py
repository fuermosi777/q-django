import unittest
from processor import basic

class Test(unittest.TestCase):

	def test_moving_average(self):
		l = [1,2,4,5,7,9,0,1,2,3]
		print(basic.moving_average(l, 3))

	def test_market_daily_return(self):
		l = [1,2,3,4,5,6,7,8,3,4]
		print(basic.market_daily_return(l))

	def test_market_cum_return(self):
		l = [1,2,3,4,5,6,7,8,3,4]
		print(basic.market_cum_return(l))

	def test_strategy_daily_return(self):
		l = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.3,0.4]
		s = [0,1,0,0,0,0,0,0,1,1]
		print(basic.strategy_daily_return(l, s))

	def test_strategy_cum_return(self):
		l = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.3,0.4]
		s = [0,1,0,0,0,0,0,0,1,1]
		print(basic.strategy_cum_return(l, s))