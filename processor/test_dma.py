import unittest
from processor import dma

class Test(unittest.TestCase):

	def set_close(self):
		self.l = [1,2,3,4,5,6,7,8,3,4,5,8,9,6,4,2,2,4,5,6]

	def test_all(self):
		self.set_close()
		print(dma.all(self.l, short=2, long=4))

	def test_result(self):
		self.set_close()
		info = dma.all(self.l, short=2, long=4)
		print(dma.result(info['close'], info['short'], info['long'], info['signal']))