import math
from processor import basic

def signal(close_list, short_list, long_list, sd):
	# s - l > SD x l ? 1 : 0
	# return a list of 1 or 0, which is the buy/sell signal
	def func():
		buy_price = None
		just_sell = False
		for idx, val in enumerate(close_list):
			if buy_price and close_list[idx] < 0.8 * buy_price:
				yield 0
				buy_price = None
				just_sell = True
			if idx > 10:
				latest_20 = [close_list[idx - x] for x in range(0, 10)]
				if close_list[idx] < min(latest_20):
					yield 0
					buy_price = None
					just_sell = True
			if short_list[idx] - long_list[idx] > sd * long_list[idx] and just_sell == False:
				yield 1
				just_sell = False
				buy_price = close_list[idx]
			else:
				buy_price = None
				yield 0
	return list(func())

def all(close_list, short=5, long=20, sd=0):
	num = len(close_list) - long + 1
	short_ma = basic.moving_average(close_list, short)[-num:]
	long_ma = basic.moving_average(close_list, long)
	close = close_list[-num:]
	return {
		'close': close,
		'short': short_ma,
		'long': long_ma,
		'signal': signal(close, short_ma, long_ma, sd)
	}

def result(close_list, short_list, long_list, signal_list):
	market_daily = basic.market_daily_return(close_list)
	market_cum = basic.market_cum_return(close_list)
	strategy_daily = basic.strategy_daily_return(market_daily, signal_list)
	strategy_cum = basic.strategy_cum_return(market_daily, signal_list)
	annualized = basic.annualized_return(strategy_daily)
	benchmark = basic.benchmark_return(market_daily)
	beta = basic.beta(strategy_daily, market_daily)
	alpha = basic.alpha(annualized, 0.045, beta, benchmark)
	return {
		'market_cum': market_cum,
		'strategy_cum': strategy_cum,
		'annualized': annualized,
		'benchmark': benchmark,
		'beta': beta,
		'alpha': alpha,
	}

def inject_date(date_list, target_list):
	return [(d, t) for (d, t) in zip(date_list, target_list)]