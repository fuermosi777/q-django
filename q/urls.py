"""q URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from api import stock, dma
from django.views.generic import TemplateView
from user import views as USER

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    # dma strategy
    url(r'^api/dma/backtesting/$', dma.backtesting),
    url(r'^api/dma/today/$', dma.today),
    # stock
    url(r'^api/stock/list/$', stock.list),
    url(r'^api/stock/single_now/$', stock.single_now),
    url(r'^api/stock/single_history/$', stock.single_history),
    url(r'^api/stock/single_today/$', stock.single_today),
]

urlpatterns += [
    url(r'^$', TemplateView.as_view(template_name='home.html')),
    url(r'^dma/$', TemplateView.as_view(template_name='dma.html')),
    url(r'^basic/$', TemplateView.as_view(template_name='basic.html')),
    url(r'^simulation/$', TemplateView.as_view(template_name='simulation.html')),
    url(r'^use/$', TemplateView.as_view(template_name='use.html')),
    url(r'^realtime/$', TemplateView.as_view(template_name='realtime.html')),
]

urlpatterns += [
    url(r'^user/auth/$', USER.auth),
]