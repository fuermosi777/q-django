from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import tushare as ts
import pandas as pd
from api.models import *
from datetime import datetime, timedelta
import pytz, time
import re
from processor import basic, dma
import itertools
from django.db.models import Q
import statistics
import chinastock as cs
from api.decorators import domain_verify

@domain_verify
def backtesting(request):
    '''
    /api/dma/backtesting/?
    	code=000001
    	short=5
    	long=20
    	sd=0.05
    	length=700
    '''
    code = request.GET.get('code', 'sh')
    short = int(request.GET.get('short', 5))
    long = int(request.GET.get('long', 20))
    sd = float(request.GET.get('sd', 0.05))
    length = int(request.GET.get('length', 700))

    stock = Stock.objects.get(code=code)
    # get last length + long - 1 prices
    prices = Price.objects.filter(stock=stock).order_by('-date')[:(length + long - 1)][::-1]
    close_list = [float(p.adj_close) for p in prices]
    date_list = [int(time.mktime(p.date.timetuple()) * 1000) for p in prices]

    # basic info
    # close price
    res = {
        'name': stock.name,
        'code': stock.code,
    }

    # moving average
    info = dma.all(close_list, short=short, long=long, sd=sd)
    result = dma.result(info['close'], info['short'], info['long'], info['signal'])
    # get date list
    date_list = date_list[-len(info['close']):]
    res['close'] = dma.inject_date(date_list, info['close'])
    res['short'] = dma.inject_date(date_list, info['short'])
    res['long'] = dma.inject_date(date_list, info['long'])
    res['signal'] = dma.inject_date(date_list, info['signal'])
    res['market_cum'] = dma.inject_date(date_list, result['market_cum'])
    res['strategy_cum'] = dma.inject_date(date_list, result['strategy_cum'])
    res['annualized'] = result['annualized']
    res['benchmark'] = result['benchmark']
    res['beta'] = result['beta']
    res['alpha'] = result['alpha']

    return JsonResponse(res)

@domain_verify
def today(request):
    '''
    /api/dma/today/?
        code=000001
        short=5
        long=20
        sd=0.05
    '''
    code = request.GET.get('code', '000001')
    short = int(request.GET.get('short', 5))
    long = int(request.GET.get('long', 20))
    sd = float(request.GET.get('sd', 0.05))

    stock = Stock.objects.get(code=code)
    prices = Price.objects.filter(stock=stock).order_by('-date')[:long][::-1]
    data = ts.get_realtime_quotes(code)
    close_list = [float(p.adj_close) for p in prices]
    close_list.append(float(data.iloc[0]['price']))
    date_list = [int(time.mktime(p.date.timetuple()) * 1000) for p in prices]

    info = dma.all(close_list, short=short, long=long, sd=sd)
    signal_yesterday = info['signal'][-2:-1][0]
    signal_today = info['signal'][-1:][0]
    signal = None
    if signal_yesterday == 0 and signal_today == 1:
        signal = 'buy'
    elif signal_yesterday == 0 and signal_today == 0:
        signal = 'sell'
    elif signal_yesterday == 1 and signal_today == 0:
        signal = 'sell'
    elif signal_yesterday == 1 and signal_today == 1:
        signal = 'hold'
    res = {
        'name': stock.name,
        'code': stock.code,
        'signal': signal,
    }

    return JsonResponse(res)