from django.db import models

class Exchange(models.Model):
    code = models.CharField(max_length=20)
    name = models.CharField(max_length=40)

    def __str__(self):
        return self.code

class Stock(models.Model):
    code = models.CharField(max_length=30, unique=True)
    name = models.CharField(max_length=40, unique=True)
    is_terminated = models.BooleanField(default=False)
    ipo_date = models.DateField()
    is_all_prices_got = models.BooleanField(default=False)
    exchange = models.ForeignKey(Exchange)

    def __str__(self):
        return self.name

class Category(models.Model):
    name = models.CharField(max_length=40,unique=True)
    shortcut = models.CharField(max_length=30,unique=True)

    def __str__(self):
        return self.name

class Stock_category(models.Model):
    stock = models.ForeignKey(Stock)
    category = models.ForeignKey(Category)

    def __str__(self):
        return self.stock.name

class Price(models.Model):
    stock = models.ForeignKey(Stock)
    date = models.DateField()
    open = models.DecimalField(max_digits=8, decimal_places=2)
    high = models.DecimalField(max_digits=8, decimal_places=2)
    close = models.DecimalField(max_digits=8, decimal_places=2)
    low = models.DecimalField(max_digits=8, decimal_places=2)
    adj_close = models.DecimalField(max_digits=8, decimal_places=2)
    volume = models.IntegerField()

    class Meta:
        unique_together = ('stock', 'date')

    def __str__(self):
        return self.stock.name