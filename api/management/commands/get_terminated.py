#!/usr/local/bin/python3.4

import sys
from django.core.management.base import BaseCommand, CommandError
from api.models import *

from django.core.mail import send_mail

import tushare as ts
import pandas as pd
from api.models import *
from datetime import datetime, timedelta
import pytz
from multiprocessing import Pool
from django.db import connection

def find_if_terminated(info):
    connection.close()
    s = info['stock']
    code = info['code']
    start = info['start']
    end = info['end']
    data = ts.get_hist_data(code, start, end)
    if data.empty:
        s.is_terminated = True
        print('%s is terminated'%code)
    else:
        s.is_terminated = False
        print('%s is back'%code)
    s.save()

class Command(BaseCommand):
    args = '<option>'
    def handle(self, *args, **options):
        trading_day = self.find_nearest_trading_day()
        stock = Stock.objects.all().order_by('id')

        infos = [{
            'stock': s,
            'code': s.code,
            'start': trading_day.strftime('%Y-%m-%d'),
            'end': trading_day.strftime('%Y-%m-%d')
        } for s in stock]

        pool = Pool(3)
        pool.map(find_if_terminated, infos)
        pool.close()
        pool.join()

    def find_nearest_trading_day(self):
        yesterday = datetime.now(pytz.timezone('Asia/Shanghai')) - timedelta(days=1)
        data = ts.get_hist_data('sh', yesterday.strftime('%Y-%m-%d'), yesterday.strftime('%Y-%m-%d'))
        if data.empty:
            dates = []
            for i in range(0, 12):
                dates.append(yesterday - timedelta(days=i))
            for d in dates:
                data = ts.get_hist_data('sh', d.strftime('%Y-%m-%d'), d.strftime('%Y-%m-%d'))
                if not data.empty:
                    return d
        else:
            return yesterday