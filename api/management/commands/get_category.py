#!/usr/local/bin/python3.4

import sys
from django.core.management.base import BaseCommand, CommandError
from api.models import *

from django.core.mail import send_mail

import tushare as ts
import pandas as pd
from api.models import *
from datetime import datetime, timedelta
import pytz

class Command(BaseCommand):
    help = ""
    def handle(self, *args, **options):
        stock_list = ts.get_zz500s()
        category = Category.objects.get(shortcut='zz500')
        for index, row in stock_list.iterrows():
            stock = Stock.objects.get(code=row['code'])
            try:
                s_c = Stock_category.objects.get(stock=stock,category=category) 
                print('already exists')  
            except: 
                s_c = Stock_category(stock=stock,category=category)
                s_c.save()
                print(s_c)
