import sys
from django.core.management.base import BaseCommand, CommandError
from api.models import *

import tushare as ts
import pandas as pd
from api.models import *
from datetime import datetime, timedelta
import pytz
from bs4 import BeautifulSoup as bs

import urllib.request

class Command(BaseCommand):
    help = ""
    def handle(self, *args, **options):
        for i in range(1, 10):
            url = 'http://vip.stock.finance.sina.com.cn/corp/view/vRPD_NewStockIssue.php?page=%s&cngem=0&orderBy=NetDate&orderType=desc'%i
            response = urllib.request.urlopen(url)
            soup = bs(response)
            table = soup.select('table#NewStockTable')
            trs = table[0].select('tr')
            for idx, val in enumerate(trs):
                if idx > 2:
                    tds = val.select('td')
                    code = tds[0].get_text()
                    name = tds[2].get_text().strip()
                    try:
                        ipo_date = datetime.strptime(tds[4].get_text() , '%Y-%m-%d')
                    except:
                        ipo_date = None
                    if ipo_date:
                        s = Stock(code=code, name=name, ipo_date=ipo_date, is_terminated=False)
                        try:
                            s.save()
                            print('%s new stock added!'%s)
                        except:
                            print('%s already exists'%s)