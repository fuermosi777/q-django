import sys
from django.core.management.base import BaseCommand, CommandError
from api.models import *

import tushare as ts
import pandas as pd
from api.models import *
from datetime import datetime, timedelta
import pytz

class Command(BaseCommand):
    help = ""
    def handle(self, *args, **options):
        stocks = ts.get_stock_basics()
        for index, row in stocks.iterrows():
            try:
                s = Stock.objects.get(code=row.name)
                s.ipo_date = datetime.strptime(str(row['timeToMarket']), '%Y%m%d')
                s.save()
                print('%s already exists, updated IPO date'%s.code)
            except:
                s = Stock(
                    code=row.name,
                    name=row['name'],
                    is_terminated=False,
                    ipo_date=datetime.strptime(str(row['timeToMarket']), '%Y%m%d'))
                s.save()
                print('%s added'%s.code)