import sys
from django.core.management.base import BaseCommand, CommandError
from api.models import *

from django.db.models import Count

import tushare as ts
import pandas as pd
from api.models import *
from datetime import datetime, timedelta
import pytz

class Command(BaseCommand):
    help = ""
    def handle(self, *args, **options):
        stock = Stock.objects.all()
        for s in stock:
            price = Price.objects.filter(stock=s).values('date').annotate(date_count=Count('date')).filter(date_count__gt=1)
            if len(price) != 0:
                for p in price:
                    price_duplicate = Price.objects.filter(stock=s,date=p['date'])
                    for idx, val in enumerate(price_duplicate):
                        if idx > 0:
                            price_duplicate[idx].delete()
                            print('price of %s on %s is deleted'%(s, price_duplicate[idx].date))

    def detect_duplicate(self):
        stock = Stock.objects.all()
        for s in stock:
            price = Price.objects.filter(stock=s).values('date').annotate(date_count=Count('date')).filter(date_count__gt=1)
            if len(price) != 0:
                print('found!')