#!/usr/local/bin/python3.4

import sys
from django.core.management.base import BaseCommand, CommandError
from api.models import *

from django.core.mail import send_mail

import tushare as ts
import pandas as pd
from api.models import *
from datetime import datetime, timedelta
import pytz
from multiprocessing import Pool
from django.db import connection

def get_data(info):
    connection.close()
    s = info['stock']
    code = s.code
    start = info['start']
    end = info['end']
    print(code,start,end)
    try:
        data = ts.get_h_data(code, autype=None, start=start, end=end)
        data_qfq = ts.get_h_data(code, start=start, end=end)
    except TypeError: # tushare bug hack
        pass
    except:
        get_data(info)
    else:
        if data is None or data_qfq is None or data.empty:
            pass
        else:
            bfq = data
            qfq = data_qfq
            for index, row in bfq.iterrows():
                p = Price(
                    stock = s,
                    date = row.name,
                    open = row['open'],
                    high = row['high'],
                    close = row['close'],
                    low = row['low'],
                    volume = row['volume'],
                    adj_close = qfq[index.strftime('%Y-%m-%d %H:%M:%S')].iloc[0]['close']
                )
                try:
                    p.save()
                    print('%s %s added!'%(s.name, p.date))
                except:
                    print('%s %s already exists'%(s.name, p.date))
            s.is_terminated = False
            s.is_all_prices_got = True
            s.save()

class Command(BaseCommand):
    args = '<option>'
    def handle(self, *args, **options):
        # if option has "redo"
        # clean up all status
        # self.record('Prices update started on %s'%str(datetime.now()))

        try:
            option = args[0]
            if option == 'today':
                target_date = datetime.now(pytz.timezone('Asia/Shanghai')) - timedelta(days=1) 
                if self.if_trading_day(target_date):
                    start = (target_date - timedelta(days=4)).strftime('%Y-%m-%d')
                    end = target_date.strftime('%Y-%m-%d')
                    stock = Stock.objects.filter(is_terminated=False)
                    infos = [{
                        'stock': s,
                        'start': start,
                        'end': end
                    } for s in stock]
                else:
                    self.record('Prices update ends on %s \n Yesterday is not trading'%(str(datetime.now())))
                    return None
        except:
            end = '2015-07-04'
            stock = Stock.objects.filter(is_terminated=False).exclude(is_all_prices_got=True)
            infos = [{
                'stock': s,
                'start': s.ipo_date.strftime('%Y-%m-%d'),
                'end': end
            } for s in stock]

        pool = Pool(3)
        pool.map(get_data, infos)
        pool.close()
        pool.join()

    def if_trading_day(self, target_date):
        index = ts.get_hist_data('sh', target_date.strftime('%Y-%m-%d'), target_date.strftime('%Y-%m-%d'))
        if index.empty:
            return False
        else:
            return True

    def record(self, msg):
        print(msg)
        send_mail('Q System Report: Prices Update', msg, 'postman@q.liuhao.im', ['liuhao1990@gmail.com'], fail_silently=False)