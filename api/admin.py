from django.contrib import admin
from api.models import *

class stockAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'is_terminated')
    search_fileds = ('code',)

class priceAdmin(admin.ModelAdmin):
    list_display = ('stock', 'date')

# Register your models here.
admin.site.register(Stock, stockAdmin)
admin.site.register(Price, priceAdmin)
admin.site.register(Category)
admin.site.register(Exchange)
admin.site.register(Stock_category)