from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import tushare as ts
import pandas as pd
from api.models import *
from datetime import datetime, timedelta
import pytz, time
import re
from processor import basic, dma
import itertools
from django.db.models import Q
import statistics
import chinastock as cs
from api.decorators import domain_verify

@domain_verify
def list(request):
    '''
    /api/stock/list/?
        category=hs300
        keyword=xxx
        limit=10
    '''
    category = request.GET.get('category', None)
    keyword = request.GET.get('keyword', None)
    limit = request.GET.get('limit', None)
    stock = Stock.objects.filter(is_terminated=False)
    if category:
        stock = stock.filter(stock_category__category__shortcut=category)
    if keyword:
        stock = stock.filter(Q(code__icontains=keyword) | Q(name__icontains=keyword))
    if limit:
        stock = stock[:int(limit)]
    res = [{
        'code': s.code,
        'name': s.name,
        'ipo_date': s.ipo_date,
    } for s in stock]
    return JsonResponse(res, safe=False)

@domain_verify
def single_now(request):
    code = request.GET.get('code', None)
    if code:
        data = ts.get_realtime_quotes(code)
        data = data.iloc[0]
        res = {
            'name': data[0],
            'open': data[1],
            'pre_close': data[2],
            'price': data[3],
            'high': data[4],
            'low': data[5],
            'bid': data[6],
            'ask': data[7],
            'volumn': data[8],
            'amount': data[9],
            'date': data[30],
            'time': data[31],
        }
    return JsonResponse(res, safe=False)

@domain_verify
def single_history(request):
    code = request.GET.get('code', None)
    if code:
        price = Price.objects.filter(stock__code=code).order_by('date')
        res = [[
            int(time.mktime(p.date.timetuple()) * 1000),
            float(p.close)
        ] for p in price]
    return JsonResponse(res, safe=False)

@domain_verify
def single_today(request):
    code = request.GET.get('code', None)
    if code:
        stock = Stock.objects.get(code=code)
        data = cs.get_stock_today(code, stock.exchange.code)
        print(code, stock.exchange.code)
        res = [[
            int(d[0].timestamp() * 1000),
            float(d[1])
        ] for d in data]
    return JsonResponse(res, safe=False)