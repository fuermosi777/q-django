(function() {
    Q.Chart = {
        drawChart: function(elem, options, settings) {
            var settingsDefault = {
                navigator: {
                    enabled: false
                },
                rangeSelector: {
                    selected: 4
                }
            };
            elem.highcharts('StockChart', {
                rangeSelector: {
                    selected: 4
                },
                yAxis: {
                    labels: {
                        formatter: function() {
                            return (this.value > 0 ? ' + ' : '') + this.value + '%';
                        }
                    },
                    plotLines: [{
                        value: 0,
                        width: 2,
                        color: 'silver'
                    }]
                },
                tooltip: {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                    valueDecimals: 2
                },
                series: options
            });
        }
    };
})();
