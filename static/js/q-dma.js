(function() {
    Q.Dma = function(elem) {
        this._ = $(elem);
        this.init();
    };

    Q.Dma.prototype.init = function() {
        this.buildElem();
        this.bind();
    };

    Q.Dma.prototype.buildElem = function() {
        var that = this;
        var className, attr;
        this._.find('*').each(function() {
            if ($(this).hasClass('elem')) {
                className = $(this).attr('class').split(' ')[0];
                attr = className.replace(/-([a-z])/g, function(g) {
                    return g[1].toUpperCase();
                });
                that[attr] = $(that._[0].querySelector('.' + className));
            }
        });
    };

    Q.Dma.prototype.writeResult = function(data) {
        var that = this;
        this.resultArea.empty();
        this.resultArea.append('' +
            '<p>' + 'Annualized return 策略年化收益率: ' + data['annualized'] + '</p>' +
            '<p>' + 'Benchmark return 基准年化收益率: ' + data['benchmark'] + '</p>' +
            '<p>' + 'Alpha: ' + data['alpha'] + '</p>' +
            '<p>' + 'Beta: ' + data['beta'] + '</p>' +
            '');
    };

    Q.Dma.prototype.bind = function() {
        var that = this;
        this.updateButton.on('click', function() {
            var sd = that.sdInput.val();
            var code = that.codeInput.val();
            var short = that.shortInput.val();
            var long = that.longInput.val();
            var length = that.lengthInput.val();
            Q.Data.getBacktesting('dma', code, short, long, sd, length)
                .then(function(data) {
                    // price chart
                    var priceOptions = [{
                        name: 'Close 复权收盘价',
                        data: data['close']
                    }, {
                        name: 'Short 短线',
                        data: data['short']
                    }, {
                        name: 'Long 长线',
                        data: data['long']
                    }];
                    Q.Chart.drawChart(that.priceChart, priceOptions);

                    // signal chart
                    var signalOptions = [{
                        name: 'Signal 信号',
                        data: data['signal']
                    }];
                    Q.Chart.drawChart(that.signalChart, signalOptions);

                    // return chart
                    var returnOptions = [{
                        name: 'Strategy 策略累积收益率',
                        data: data['strategy_cum']
                    }, {
                        name: 'Market 基准累积收益率',
                        data: data['market_cum']
                    }];
                    Q.Chart.drawChart(that.returnChart, returnOptions);

                    that.writeResult(data);
                });
        });
    };
})();
