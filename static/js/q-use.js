(function() {
    Q.Use = function(elem) {
        this._ = $(elem);
        this.init();
    };

    Q.Use.prototype.init = function() {
        this.collectElem();
        this.bind();
    };

    Q.Use.prototype.collectElem = function() {
        this.startButton = this._.find('button.start-button');
        this.buyList = this._.find('.buy-list');
        this.sellList = this._.find('.sell-list');
        this.holdList = this._.find('.hold-list');
        this.buySignalNum = 0;
        this.sellSignalNum = 0;
        this.holdSignalNum = 0;
        this.currentNum = 0;
        this.totalNum = 0;
        this.progressBar = this._.find('.progress-bar');
    };

    Q.Use.prototype.bind = function() {
        var that = this;
        this.startButton.on('click', function() {
            that.startButton.attr('disabled', 'disabled');
            Q.Data.getStockList()
                .then(function(data) {
                    that.totalNum = data.length;
                    that.startButton.text('Got ' + data.length + ' stocks');
                    for (var i = 0; i < data.length; i++) {
                        Q.Data.getDmaSignal(data[i]['code'])
                            .then(function(data) {
                                that.outputSignal(data);
                            });
                    }
                });
        });
    };

    Q.Use.prototype.outputSignal = function(data) {
        var that = this;
        var signal = data['signal'];
        this[signal + 'List'].append('<div class="mdl-card__supporting-text mdl-card__border">' + data['code'] + ' ' + data['name'] + '</div>');
        // signal num
        this[signal + 'SignalNum'] += 1;
        this[signal + 'List'].find('.badge').text(this[signal + 'SignalNum']);
        this.currentNum += 1;
        this.progressBar[0].MaterialProgress.setProgress(that.currentNum / that.totalNum * 100);
    };
}());
