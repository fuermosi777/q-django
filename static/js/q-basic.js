(function() {
    Q.Basic = function(elem) {
        this._ = $(elem);
        this.init();
        this.searchDone = true;
        this.priceDone = true;
    };

    Q.Basic.prototype.init = function() {
        this.collectElem();
        this.bind();
    };

    Q.Basic.prototype.collectElem = function() {
        this.searchInput_ = this._.find('input.search-input');
        this.stockList_ = this._.find('.stock-list');
        this.priceChart = this._.find('.price-chart');
        this.now = this._.find('.now');
    };

    Q.Basic.prototype.getStockList = function(keyword) {
        var that = this;
        var url = '/api/stock/list/?keyword=' + keyword + '&limit=10';
        this.searchDone = false;
        var promise = new Promise(function(resolve, reject) {
            $.getJSON(url, function(data) {
                resolve(data);
                that.searchDone = true;
            });
        });
        return promise;
    };

    Q.Basic.prototype.bind = function() {
        var that = this;
        this.searchInput_.on('input', function() {
            var keyword = $(this).val();
            if (that.searchDone === true) {
                var processor = that.getStockList(keyword);
                processor.then(function(data) {
                    that.buildStockList(data);
                });
            }
        });

        $('body').on('click', '.stock-item', function(e) {
            if (that.priceDone === true) {
                e.preventDefault();
                var code = $(this).data('code');
                var getPriceData = that.getPriceData(code);
                var getNowData = that.getNowData(code);
                getPriceData.then(function(data) {
                    var priceOptions = [{
                        name: 'Close Price',
                        data: data
                    }];
                    that.drawChart(that.priceChart, priceOptions);
                });
                getNowData.then(function(data) {
                    that.now.empty();
                    that.now.append('' +
                        '<span style="color:gray">' + data['name'] + '</span> ' +
                        '<span style="font-weight: bold; color:' + (data['price'] - data['pre_close'] > 0 ? 'red' : 'green') + '">' + data['price'] + '</span> ' +
                        '<span>Pre Close 昨收: ' + data['pre_close'] + '</span> ' +
                        '<span>Open 开盘: ' + data['open'] + '</span> ' +
                        '<span>High 最高: ' + data['high'] + '</span> ' +
                        '<span>Low 最低: ' + data['low'] + '</span> ' +
                        '');
                });
            }
        });
    };

    Q.Basic.prototype.buildStockList = function(data) {
        var that = this;
        this.stockList_.empty();
        for (var i = 0; i < data.length; i++) {
            this.stockList_.append('<div class="mdl-card__actions"><a href="#" class="mdl-button stock-item" data-code="' + data[i]['code'] + '">' + data[i]['code'] + '&nbsp;&nbsp;&nbsp;' + data[i]['name'] + '</a></div>');
        };
    };

    Q.Basic.prototype.drawChart = function(elem, options) {
        var that = this;
        elem.highcharts('StockChart', {
            rangeSelector: {
                selected: 4
            },
            yAxis: {
                labels: {
                    formatter: function() {
                        return (this.value > 0 ? ' + ' : '') + this.value + '%';
                    }
                },
                plotLines: [{
                    value: 0,
                    width: 2,
                    color: 'silver'
                }]
            },
            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                valueDecimals: 2
            },
            series: options
        });
    };

    Q.Basic.prototype.getPriceData = function(code) {
        var that = this;
        this.priceDone = false;
        var promise = new Promise(function(resolve, reject) {
            url = '/api/stock/single_history/?code=' + code;
            $.getJSON(url, function(data) {
                resolve(data);
                that.priceDone = true;
            });
        });
        return promise;
    };

    Q.Basic.prototype.getNowData = function(code) {
        var promise = new Promise(function(resolve, reject) {
            url = '/api/stock/single_now/?code=' + code;
            $.getJSON(url, function(data) {
                resolve(data);
            });
        });
        return promise;
    };
})();
