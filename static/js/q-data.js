(function() {
    Q.Data = {
        getBacktesting: function(strategy, code, short, long, sd, length) {
            var url = '/api/' + strategy + '/backtesting/?code=' + code + '&short=' + short + '&long=' + long + '&sd=' + sd + '&length=' + length;
            var promise = new Promise(function(resolve, reject) {
                $.getJSON(url, function(data) {
                    resolve(data);
                });
            });
            return promise;
        },
        getStockList: function(keyword) {
            var url = '/api/stock/list/';
            if (keyword) {
                url += '?keyword=' + keyword + '&limit=10';
            }
            var promise = new Promise(function(resolve, reject) {
                $.getJSON(url, function(data) {
                    resolve(data);
                });
            });
            return promise;
        },
        getStockNow: function(code) {
            var promise = new Promise(function(resolve, reject) {
                url = '/api/stock/single_now/?code=' + code;
                $.getJSON(url, function(data) {
                    resolve(data);
                });
            });
            return promise;
        },
        getStockHistory: function(code) {
            var promise = new Promise(function(resolve, reject) {
                url = '/api/stock/single_history/?code=' + code;
                $.getJSON(url, function(data) {
                    resolve(data);
                });
            });
            return promise;
        },
        getDmaSignal: function(code) {
            var promise = new Promise(function(resolve, reject) {
                url = '/api/dma/today/?code=' + code;
                $.getJSON(url, function(data) {
                    resolve(data);
                });
            });
            return promise;
        },
        getStockToday: function(code) {
            var promise = new Promise(function(resolve, reject) {
                url = '/api/stock/single_today/?code=' + code;
                $.getJSON(url, function(data) {
                    resolve(data);
                });
            });
            return promise;
        }
    };
})();
