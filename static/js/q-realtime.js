(function() {
    Q.Realtime = function(elem) {
        this._ = $(elem);
        this.init();
    };

    Q.Realtime.prototype.init = function() {
        this.collectElem();
        this.loginPanel.hide();
        this.realtimePanel.show();
        this.work();
        this.bind();
    };

    Q.Realtime.prototype.collectElem = function() {
        this.loginPanel = this._.find('.login-panel');
        this.realtimePanel = this._.find('.realtime-panel');
        this.loginButton = this._.find('.login-button');
        this.loginError = this._.find('.login-error');
        this.username = null;
        this.password = null;
        this.codeInput = this._.find('.code-input');
        this.stockResultList = this._.find('.stock-result-list');
        this.terminal = this._.find('.terminal');
        this.stockTable = this._.find('.stock-table');
        this.stocks = [];
        this.runningWorkers = [];
        this.strategies = [];
    };

    Q.Realtime.prototype.isLogin = function() {
        return this.username ? true : false;
    };

    Q.Realtime.prototype.auth = function(username, password) {
        var url = '/user/auth/';
        var param = {
            username: username,
            password: password
        };
        var promise = new Promise(function(resolve, reject) {
            $.post(url, param)
                .done(function(data) {
                    console.log('auth success');
                    resolve(data);
                })
                .fail(function() {
                    console.log('auth fail');
                    reject('Error');
                });
        });
        return promise;
    };

    Q.Realtime.prototype.bind = function() {
        var that = this;
        this.loginButton.on('click', function() {
            var username = that._.find('input.username').val();
            var password = that._.find('input.password').val();
            that.auth(username, password).then(function(data) {
                that.loginError.hide();
                return that.setCookie(data['username'], data['password']);
            }).catch(function() {
                that.loginError.show();
            }).then(function() {
                that.loginPanel.hide();
                that.realtimePanel.show();
            });
        });

        // search stock
        this.codeInput.on('input', function() {
            var keyword = $(this).val();
            if (keyword === '') {
                that.stockResultList.empty();
                return;
            }
            var processor = Q.Data.getStockList(keyword).then(function(data) {
                that.output('Search result: ' + data.length + ' stocks found');
                that.stockResultList.empty();
                for (var i = 0; i < data.length; i++) {
                    that.stockResultList.append('<a href="#" class="mdl-button stock-item" data-name="' + data[i]['name'] + '" data-code="' + data[i]['code'] + '">' + data[i]['code'] + '&nbsp;&nbsp;&nbsp;' + data[i]['name'] + '</a>');
                }
            });
        });

        // click item in stock result list
        $('body').on('click', '.stock-item', function() {
            var code = $(this).data('code');
            var name = $(this).data('name');
            if (that.stocks.indexOf(code) !== -1) {
                return;
            }
            that.stockTable.find('tbody').append('<tr data-code="' + code + '"><td class="action"><button data-code="' + code + '" class="delete-button" style="height:initial; font-size: initial">-</button></td><td class="code">' + code + '</td><td class="name">' + name + '</td><td class="price"></td><td class="dma"></td></tr>');
            that.stocks.push(code);
        });

        // delete stock
        $('body').on('click', '.delete-button', function() {
            var code = $(this).data('code');
            that.stockTable.find('tbody tr[data-code="' + code + '"]').remove();
            that.stocks.splice(that.stocks.indexOf(code), 1);
            that.runningWorkers.splice(that.stocks.indexOf(code), 1);
        });

        // click table row
        $('body').on('click', 'tbody tr', function() {
            var code = $(this).data('code');
            Q.Data.getStockToday(code)
                .then(function(data) {
                    var options = [];
                    options.push({
                        name: 'Realtime',
                        data: data
                    });
                    Q.Chart.drawChart($('.realtime-chart'), options);
                });
        });
    };

    Q.Realtime.prototype.setCookie = function(username, password) {
        var that = this;
        return new Promise(function(resolve, reject) {
            that.username = username;
            that.password = password;
            resolve();
        });
    };

    Q.Realtime.prototype.logout = function() {
        this.username = null;
        this.password = null;
        this.loginPanel.show();
        this.realtimePanel.hide();
    };

    Q.Realtime.prototype.output = function(words) {
        var commands = this.terminal.html().split('<br>');
        if (commands.length > 15) {
            commands.shift();
            this.terminal.html(commands.join('<br>'));
        }
        this.terminal.append(words + '<br>');
    };

    Q.Realtime.prototype.work = function() {
        var that = this;
        for (var i = 0; i < this.stocks.length; i++) {
            var code = this.stocks[i];
            if (this.runningWorkers.indexOf(code) === -1) {
                this.startWorker(code);
            }
        }
        setTimeout(function() {
            that.work();
        }, 1000);
    };

    Q.Realtime.prototype.startWorker = function(code) {
        this.runningWorkers.push(code);
        var that = this;
        var getStockNow = Q.Data.getStockNow(code);
        var getDmaSignal = Q.Data.getDmaSignal(code);
        Promise.all([getStockNow, getDmaSignal])
            .then(function(dataset) {
                var priceData = dataset[0];
                var dmaData = dataset[1];
                // price
                that.output('Get stock realtime data: ' + code);
                var color = priceData['price'] > priceData['open'] ? 'red' : 'green';
                that.stockTable.find('tbody tr[data-code=' + code + '] td.price').html('<span class="mdl-color-text--' + color + '">' + priceData['price'] + ' (' + ((priceData['price'] - priceData['open']) / priceData['open'] * 100).toFixed(2) + '%)' + '</span>');
                // dma
                that.output('Get stock dma signal: ' + code);
                that.stockTable.find('tbody tr[data-code=' + code + '] td.dma').html(dmaData['signal']);
                // go on
                if (that.stocks.indexOf(code) !== -1) {
                    setTimeout(function() {
                        that.startWorker(code);
                    }, 5000);
                }
            });
    };
}());
