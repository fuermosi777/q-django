(function() {
    Q.Simulation = function(elem) {
        this._ = $(elem);
        this.result = [];
        this.diff_sum = 0;
        this.avg = $('<p style="color:red"></p>');
        this.length = 0;
    };

    Q.Simulation.prototype.setStrategy = function(strategy) {
        this.strategy = strategy;
    };

    Q.Simulation.prototype.setParam = function(param) {
        this.param = param;
    };

    Q.Simulation.prototype.getStocks = function(category) {
        var that = this;
        var url = '/api/stock/list/?category=' + category;
        var promise = new Promise(function(resolve, reject) {
            that._.empty();
            that.output('loading...');
            $.getJSON(url, function(data) {
                that._.empty();
                that.output('get ' + data.length + ' stocks');
                resolve(data);
            });
        });
        return promise;
    };

    Q.Simulation.prototype.simulate = function(data) {
        var that = this;
        var processor;
        this._.append(that.avg);
        this.length = data.length;
        // simulate
        $.each(data, function(index, value) {
            processor = that.getResult(value['code']);
            if (index + 1 !== data.length) {
                processor.then(that.getResult(data[index + 1]['code']));
            }
        });
    };

    Q.Simulation.prototype.getResult = function(code) {
        var that = this;
        var url = '/api/' + this.strategy + '/backtesting/?code=' + code + '&' + this.param;
        var promise = new Promise(function(resolve, reject) {
            $.getJSON(url, function(data) {
                // end
                that.output(data['code'] + '&#09;' + data['annualized'] + '&#09;' + data['benchmark']);
                that.result.push({
                    code: data['code'],
                    a_return: data['annualized'],
                    b_return: data['benchmark']
                });
                that.diff_sum = that.diff_sum + parseFloat(data['annualized']) - parseFloat(data['benchmark']);
                that.avg.text('AVG abnormal return: ' + that.diff_sum / that.length);
                resolve();
            });
        });
        return promise;
    };

    Q.Simulation.prototype.startover = function() {
        this.result = [];
        this.diff_sum = 0;
        this.avg = $('<p style="color:red"></p>');
        this.length = 0;
        this._.empty();
    };

    Q.Simulation.prototype.output = function(words) {
        this._.append('<p>' + words + '</p>');
    };
}());
