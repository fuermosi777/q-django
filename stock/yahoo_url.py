BASE_URL = 'http://query.yahooapis.com/v1/public/yql?q='
SUFFIX = '&env=store://datatables.org/alltableswithkeys&format=json'

def hist_data_url(code, start, end):
    return (BASE_URL + 'select * from yahoo.finance.historicaldata where symbol = "%s" and startDate = "%s" and endDate = "%s"'%(code, start, end) + SUFFIX).replace(' ','%20')