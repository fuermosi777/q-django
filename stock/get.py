import stock.yahoo_url
import json
import urllib.request
from datetime import datetime, timedelta

def historical_data(code, start, end):
    url = stock.yahoo_url.hist_data_url(code, start, end)
    response = urllib.request.urlopen(url)
    data = json.loads(response.read().decode('utf-8'))
    return yahoo_data_to_result(data)

def latest_data(code):
    start = (datetime.now() - timedelta(days=6)).strftime('%Y-%m-%d')
    end = datetime.now().strftime('%Y-%m-%d')
    print(code,start,end)
    url = stock.yahoo_url.hist_data_url(code, start, end)
    response = urllib.request.urlopen(url)
    data = json.loads(response.read().decode('utf-8'))
    return yahoo_data_to_result(data)

def yahoo_data_to_result(data):
    if not data:
        return False
    else:
        data = data['query']['results']['quote']
        res = [{
            'date': d['Date'],
            'open': d['Open'],
            'high': d['High'],
            'low': d['Low'],
            'close': d['Close'],
            'adj_close': d['Adj_Close'],
        } for d in data]
        return res