from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from api.models import *
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login

@csrf_exempt
def auth(request):
    username = request.POST.get('username', None)
    password = request.POST.get('password', None)
    if not username or not password:
        return HttpResponse(status=503)
    user = authenticate(username=username, password=password)
    if user is not None and user.is_active:
        res = {
            'username': username,
            'password': password
        }
        return JsonResponse(res)
    else:
        return HttpResponse(status=503)