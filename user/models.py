from django.db import models
from django.contrib.auth.models import User
from api.models import Stock

class Portfolio(models.Model):
    user = models.ForeignKey(User)
    stock = models.ForeignKey(Stock)

    def __str__(self):
        return self.user.username